<?php
function SendNotification($id,$data,$kind,$FIREBASE_API_KEY){
    // Enabling error reporting
    error_reporting(-1);
    ini_set('display_errors', 'On');

    require_once __DIR__ . '/models/firebase.php';
    require_once __DIR__ . '/models/push.php';

    $firebase = new Firebase($FIREBASE_API_KEY);
    $push = new Push();

    // optional payload
    $payload = array();
    $payload['kind'] = $kind;
    $payload['data'] = $data['data'];

    // notification title
    $title = isset($data['title']) ? $data['title'] : '';

    // notification message
    $message = isset($data['message']) ? $data['message'] : '';

    // push type - single user / topic
    //  $push_type = isset($_GET['push_type']) ? $_GET['push_type'] : '';

    // whether to include to image or not
    $include_image = '';


    $push->setTitle($title);
    $push->setMessage($message);

    $push->setImage($include_image);

    $push->setIsBackground(TRUE);
    $push->setPayload($payload);

    $json = '';
    $response = '';

    $json = $push->getPush();
    $regId = $id;
    $response = $firebase->send($regId, $json);

    if ($response == false) {
        // try {
            //If FCM fails try APNS assuming $regId is the device token and assuming the package is installed correctly.
            //Package info on https://github.com/davibennun/laravel-push-notification
            //Set the message to try pushing with APNS
            $ios_notification = \PushNotification::Message($message,[
                'custom' => $data
            ]);
            //It throws Exception when fails
            if (isset($data['app']) && $data['app'] == 1) {
                $notification = \PushNotification::app(['environment' => 'development',
                'certificate' => app_path('development_1.pem'),
                'passPhrase'  => '',
                'service'     => 'apns'])
                ->to(strtolower($regId))
                ->send($ios_notification);
            }else{
                $notification = \PushNotification::app(['environment' => 'development',
                'certificate' => app_path('development_2.pem'),
                'passPhrase'  => '',
                'service'     => 'apns'])
                ->to(strtolower($regId))
                ->send($ios_notification);
            }
            dd($notification);
        // } catch (\Exception $e) {
            //If fails return false if not it'll return true eventually
        //     return false;
        // }
    }
    return true;
}


$notificationData['title']=$_GET['title'];
$notificationData['message']=$_GET['message'];
$notificationData['data']=$_GET['data'];
$notificationData['app']=$_GET['app'];
$regIDS= $_GET['reg_id'];
$addnotification="";
for ($i=0; $i < count($regIDS) ; $i++) {
    # code...
    $addnotification= SendNotification($regIDS[$i],$notificationData,$_GET['kind'],$_GET['FIREBASE_API_KEY']);

}

//print_r($regIDS);
//echo json_encode($regIDS);
echo $addnotification;


?>
